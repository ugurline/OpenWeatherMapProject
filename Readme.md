## Synopsis

Purpose of this project is to run automated tests for openweathermap.org API. In my tests I achieve %100 capability for API functionality. 
I also almost fully cover the data validity, but not fully because of missing/wrong data documentation at openweathermap.org, such as coordinates covarage, city names etc.
Project contains these endpoints:

#### /data/2.5/weather?
#### /data/2.5/box/city?
#### /data/2.5/find?
#### /data/2.5/group?

## Code Example
Each endpoint has its own REST request with relevant name.

There's one Test Suit -testWeatherResponses- which uses the defined REST requests in each test case.

Each test case has its own assertions as groovy scripts or as JsonPATH existence method.


## Motivation
Written upon request by ADIDAS

## Installation
This is a SOAP-UI project. I wrote it using SOAP-UI 5.4.0 tool. 

This is freeware version, which is less capable than SOAP-UI Pro but I managed to deliver the need.

To be able to run the tests, tester needs to pull the project to local and use import function in SOAP-UI 5.4.0 tool.

Import Project-1-soapui-project.xml for running the tests.

Project also contains data-source files I used for using city names, ids and zip-codes in requests.

City names and ids are taken from original json file city.list.json obtained from openweathermap.org.

I made [another project](https://gitlab.com/ugurline/JSON_parser) to parse this huge json file to fetch city names and ids only, and creating seperate files for use.


## API Reference


## Tests
To run all tests simultaneously, after importing the Project-1-soapui-project.xml file, double click to testWeatherResponses Test Suit and click Play button. 
Each test case will run, and when they're finished, they turn green for successfull, and red for failed results. Tester can double click on each test case to go into them
and examine the fail results reasons.
Each test case has one positive and one negative scenario to cover successfull and failed responses.
You can refer to **howToRunTestSuit.jpg** to get the idea.


## Known Issues
There are couple of issues, related to test data:

1.testWeatherByCityName test case fails from time to time, because city doesn't exist in API response, even though all cities are taken from provider's own data-file.
This actually is either bug or data mismatch on provider's side. Example : Kosh

2.Some cities have multiple or differently written names. Example : Guliston in the request, Gulistan in response.
Tester can disable the 1st assertion "Script Assertion - Check if city name is same in response as request" in testWeatherByCityName test case in order to get over this issue.

3.Most of the zip-codes in the world doesn't exist in provider's database. And they don't provide available zip-codes in their DB. So I decided to go with a small data-set.

4.Some coordinates don't have cities around, so they give empty Json in the response. Because of this I narrowed down the coordinate rectengle range only for USA.


## Contributors
Ugur Aydin
